# Recruitment Django Application

This is a simple recruitment app built using Django 2.0+.

## Features

- Django 2.0+

## For Production Deployment

- PostgreSQL database support with psycopg2.

## How to install and run the Application

Python 3.0+ is required to run the application

Create and Activate virtual environment
```
(On windows):
$ python -m venv myvenv
$ myvenv/Scripts/activate

(On Linux or macOS):
$ sudo pip3 install virtualenv 
$ virtualenv venv 
$ source venv/bin/activate

```

Install requirements:
```
$ pip install -r requirements.txt
```

Run the application
```
$ python manage.py runserver
```


## Credentials for admin account

- username: admin
- password: adminlogin

## Deployment

It is possible to deploy to Heroku or to your own server.

### Heroku

```bash
$ heroku create
$ heroku addons:add heroku-postgresql:recruitment-dev
$ heroku pg:promote DATABASE_URL
$ heroku config:set ENVIRONMENT=PRODUCTION
$ heroku config:set DJANGO_SECRET_KEY=`./manage.py generate_secret_key`
```