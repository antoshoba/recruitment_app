from django import forms

class JobForm(forms.Form):
    name = forms.CharField(max_length=150, required=True)
    emailaddress = forms.EmailField(max_length=70, required=True)
    phone = forms.IntegerField(required=True)
    message = forms.CharField(required=False)
    job_id = forms.IntegerField()
    resume_file = forms.FileField(required=True)
