from django.db import models

class Jobs(models.Model):
    title = models.CharField(max_length = 150)
    company_name = models.CharField(max_length = 150)
    city = models.CharField(max_length = 50)
    job_time = models.CharField(max_length = 50, default='')
    salary = models.CharField(max_length = 150, default='')
    job_description = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-created',)

    # def __str__(self):
    #     return self.title

class JobApplication(models.Model):
    name = models.CharField(max_length = 50)
    email = models.EmailField(max_length=70, unique= True)
    phone = models.CharField(max_length=25)
    resume_file = models.FileField(upload_to='resume')
    message = models.TextField(blank=True, null= True)
    status = models.CharField(max_length = 15, blank=True, null= True)
    jobs = models.ForeignKey(Jobs, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-created',)

    # def __str__(self):
    #     return self.name
