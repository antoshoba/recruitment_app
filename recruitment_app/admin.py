from django.contrib import admin
from .models import JobApplication, Jobs

admin.site.register(JobApplication)
admin.site.register(Jobs)
