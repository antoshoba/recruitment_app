from django.shortcuts import render
from .models import Jobs, JobApplication
from .forms import JobForm
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required

def index(request):
    data = Jobs.objects.all()
    return render(request,'recruitment/index.html', {'job_details':data})

# @login_required
def recruiter_details(request):
    data = Jobs.objects.all()
    return render(request,'recruitment/recruiter_details.html', {'job_details':data})

def recruiter_details_view(request, id):
    data = JobApplication.objects.filter(jobs_id=id)
    return render(request,'recruitment/recruiter_details_view.html', {'job_details':data})

def user_login(request):
    if request.method == 'POST':
        username = request.POST['user_name']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            print("success!")
            login(request, user)
            return HttpResponseRedirect('/list_recruiter/')
        else:
           return render(request, 'auth/login.html')

    else:
        return render(request, 'auth/login.html')

def recruiter(request, id):
    if request.method == 'POST':
        form = JobForm(request.POST, request.FILES)
        if form.is_valid():
            instance = JobApplication(resume_file=request.FILES['resume_file'])
            instance.name = form.cleaned_data['name']
            instance.email  = form.cleaned_data['emailaddress']
            instance.phone  = form.cleaned_data['phone']
            instance.message  = form.cleaned_data['message']
            # get job instance
            job = Jobs.objects.get(pk=form.cleaned_data['job_id'])
            instance.jobs = job
            instance.save()
            return HttpResponseRedirect('/')

    return render(request,'recruitment/recruiter.html', {'id':id})

def change_status(request, id, status):
    job = JobApplication.objects.filter(pk=id).update(status=status)
    print(job, status)
    return HttpResponseRedirect('/list_recruiter')


